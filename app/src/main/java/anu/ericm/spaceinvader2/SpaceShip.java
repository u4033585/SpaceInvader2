
package anu.ericm.spaceinvader2;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * SpaceShip - this is a simple space ship that shoots missiles
 * Eric McCreath 11/03/16.
 */
public class SpaceShip extends Sprite {

    public static final float SHIPWIDTH = (1.0f / 20.0f);
    public static final float STARTX = 0.5f;
    public static final float STARTY = 0.9f;

    public SpaceShip() {
        pos = new Pos(STARTX,STARTY);
    }

    public void draw(Canvas c , Paint p) {
        int h = c.getHeight();
        int w = c.getWidth();

        float xc = pos.x * w;
        float yc = pos.y * h;
        float r = SHIPWIDTH * w;

        c.drawLine(xc, yc - r, xc+r/2.0f, yc + r, p);
        c.drawLine(xc, yc - r, xc-r/2.0f, yc + r, p);
        c.drawLine(xc-r/2.0f, yc + r, xc+r/2.0f, yc + r, p);
    }


    public boolean hitby(Bomb b) {
        return (b.pos.distance(pos) < SHIPWIDTH);
    }
}
