package anu.ericm.spaceinvader2;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Created by ericm on 16/03/16.
 */
public class Aliens extends ArrayList<Alien> {
    public static final float DOWNSTEP = 0.01f;
    public static final float LRSTEP = 0.01f;
    public static final double PROBOFBOMB = 0.01;

    private boolean movingleft;
    private Random rand = new Random();


    public static Aliens gridAliens(int cols, int rows) {
        Aliens res = new Aliens();
        float xgap = 1.0f / (cols + 1);
        for (float y = xgap; y < (rows + 0.5f) * xgap; y += xgap) {
            for (float x = xgap; x < (1.0f - xgap / 2.0f); x += xgap) {
                res.add(new Alien(x, y));
            }
        }
        res.movingleft = true;
        return res;
    }

    public void draw(Canvas canvas, Paint paint) {
        for (Alien a : this) a.draw(canvas, paint);
    }

    public void step() {
        // move the aliens side to side
        float max = Game.MINXY;
        float min = Game.MAXXY;
        for (Alien a : this) {
            if (a.pos.x > max) max = a.pos.x;
            if (a.pos.x < min) min = a.pos.x;
        }
        if (movingleft) {
            if (max > Game.MAXXY) {
                movingleft = false;
                for (Alien a : this) {
                    a.pos.y += DOWNSTEP;
                }
            }
        } else {
            if (min < Game.MINXY) {
                movingleft = true;
                for (Alien a : this) {
                    a.pos.y += DOWNSTEP;
                }
            }
        }
        for (Alien a : this) {
            a.pos.x += (movingleft ? LRSTEP : -LRSTEP);
        }
    }

    public Bombs getBombs() {
        Bombs res = new Bombs();
        for (Alien a : this) {
            if (rand.nextDouble() < PROBOFBOMB) res.add(new Bomb(a.pos));
        }
        return res;
    }

    public void removeHit(Missiles ms) {
        Iterator<Alien> ai = this.iterator();
        while (ai.hasNext()) {
            Alien a = ai.next();
            if (a.hitby(ms)) ai.remove();
        }
    }

}
