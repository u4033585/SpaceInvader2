package anu.ericm.spaceinvader2;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by ericm on 16/03/16.
 */
public class Bombs extends ArrayList<Bomb> {

    public static final float BOMBSTEP = 0.03f;

    public void step() {
        for (Bomb b : this) b.pos.y += BOMBSTEP;
        Iterator<Bomb> bi = this.iterator();
        while (bi.hasNext()) {
            Bomb b = bi.next();
            if (b.pos.y > Game.MAXXY) bi.remove();
        }
    }

    public void draw(Canvas canvas, Paint paint) {
        for (Bomb b : this) b.draw(canvas, paint);
    }
}
