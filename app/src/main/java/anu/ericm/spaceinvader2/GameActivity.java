package anu.ericm.spaceinvader2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
/*
     GameActivity - this is the activity that holds the main game.
     Eric McCreth GPL 2017
 */

public class GameActivity extends AppCompatActivity implements  GameOver {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        SpaceView spaceView = (SpaceView) findViewById(R.id.spaceview);
        spaceView.registerGameOver(this);

    }

    @Override
    public void gameOver() {
        setResult(AppCompatActivity.RESULT_OK);
        finish();
    }
}
