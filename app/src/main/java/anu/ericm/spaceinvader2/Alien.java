
package anu.ericm.spaceinvader2;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;

/**
 * Alien - this is a simple alien sprite
 * Eric McCreath 11/03/16.
 */
public class Alien extends Sprite {

    public static final float ALIENRADIUS = (1.0f / 30.0f);

    public Alien(float x, float y) {
        this.pos = new Pos(x,y);
    }

    // draw the alien
    public void draw(Canvas c , Paint p) {
        int h = c.getHeight();
        int w = c.getWidth();

        float xc = pos.x * w;
        float yc = pos.y * h;
        float r = w* ALIENRADIUS;
        c.drawCircle(xc, yc, r, p);
    }

    // determine if any missile hits this Alien
    public boolean hitby(ArrayList<Missile> missiles) {
        for (Missile m : missiles) {
            if (m.pos.distance(pos) < 1.0f/30.0f) return true;
        }
        return false;
    }


}
